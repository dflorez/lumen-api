<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Users extends Model {

   protected $table = 'users';

    /**
     * The cast attributes for the model's.
     *
     * @var array
     */
    protected $casts = [
        'updated_at'  => 'datetime:Y-m-d H:i:s',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The rules attributes for the model's.
     *
     * @var array
     */
   public static $rules = [
		"name" => "required"
	];

	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

   protected $fillable = ['id', 'name', 'is_active', 'created_at', 'updated_at'];

     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


}
