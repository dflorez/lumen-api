<?php

namespace App\Exceptions;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionTypeHandler
{
    public static function generalResponse($exception, $request)
    {

        $responseData = [
            'data' => [
                'is_error' => [
                    'code' => 500,
                    'message' => $exception->getMessage(),
                    'url' => ""
                ],
                'has_error' => [
                    'code' => 500,
                    'message' => $exception->getMessage(),
                    'detail' => $exception->getMessage()
                ]
            ]
        ];

        if ($exception instanceof HttpException) {
            $responseData = self::HttpExceptionHandler($exception);
        } elseif ($exception instanceof FatalThrowableError) {
            $responseData = self::FatalThrowableErrorHandler($exception);
        } elseif ($exception instanceof ValidationException) {
            $responseData = self::ValidationExceptionHandler($exception);
        }

        return response()->json($responseData);
    }

    private static function HttpExceptionHandler($exception)
    {
        $className = get_class($exception);
        $className = explode('\\', $className);
        $message = end($className) . ' on ' . Request::url();

        if (!env('APP_DEBUG')) {
            switch ($exception->getStatusCode()) {
                case 400:
                    $message = 'Bad Request! Your API request is invalid.';
                    break;
                case 401:
                    $message = 'Unauthorized! Your API key is wrong.';
                    break;
                case 404:
                    $message = 'Not Found! The specific API could not be found.';
                    break;
            }
        }

        if ($exception->getMessage())
            $message = $exception->getMessage();

        return [
            'data' => [
                'is_error' => [
                    'code' => $exception->getStatusCode(),
                    'message' => $message,
                    'url' => ""
                ],
                'has_error' => [
                    'code' => 500,
                    'message' => $message,
                    'detail' => $message
                ]
            ]
        ];
    }

    private static function FatalThrowableErrorHandler($exception)
    {
        if (env('APP_DEBUG')) {
            $message = $exception->getOriginalClassName()
                . ': ' . $exception->getMessage()
                . ' in ' . $exception->getFile()
                . ' on Line ' . $exception->getLine();
        } else {
            $message = 'Internal Server Error! We had a problem with our server. Try again later.';
        }

        return [
            'data' => [
                'is_error' => [
                    'code' => 500,
                    'message' => $exception->getMessage(),
                    'url' => ""
                ],
                'has_error' => [
                    'code' => 500,
                    'message' => $exception->getMessage(),
                    'detail' => $exception->getMessage()
                ]
            ]
        ];
    }

    private static function ValidationExceptionHandler($exception)
    {
        return [
            'data' => [
                'is_error' => [
                    'code' => 500,
                    'message' => $exception->getMessage(),
                    'url' => ""
                ],
                'has_error' => [
                    'code' => 500,
                    'message' => Arr::flatten($exception->errors()),
                    'detail' => $exception->errors()
                ]
            ]
        ];
    }
}
