<?php namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;


trait RESTActions {

    protected $statusCodes = [
        'done' => 200,
        'created' => 201,
        'not_found' => 404,
        'removed' => 204,
    ];

    public function all(Request $request)
    {
        $m = self::MODEL;
        if (count($m::all()) > 0){
            $columns = $request->getContent();
            if (empty($columns))
                return $this->responseSuccess('done', $m::all(), $m);
            else
                return $this->responseSuccess('done', $m::all(json_decode($columns)), $m);

        }
        else
            return $this->responseError('not_found', $m::all(), $m);
    }

    public function read($id, Request $request)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        if(is_null($model)){
            return $this->responseError('not_found',$id, $m);
        }else{
            $columns = $request->getContent();

            if (!empty($columns))
                return $this->responseSuccess('done', $model::where('id',$id)
                    ->get(json_decode($columns)),$m);
            else
                return $this->responseSuccess('done', $model, $m);

        }

    }

    public function create(Request $request)
    {
        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        $m = $m::where('name', $request->request->get('name'));
        if (!$m->exists()){
            $request->request->add(['is_active' => "1"]);
            return $this->responseSuccess('created', $m->create($request->all()),$m);
        }
        else{
            return $this->responseError('done', $m->get(),$m, __('messages.registro_existe'));
        }
    }

    public function update(Request $request, $id)
    {

        $m = self::MODEL;
        $this->validate($request, $m::$rules);
        $model = $m::find($id);
        if(is_null($model)){
            return $this->responseError('not_found', $model,$m,__('messages.registro_no_existe'));
        }else{
            $request->request->add(['updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
            $model->update($request->all());
        }
        return $this->responseSuccess('done', $model, $m);
    }

    public function delete($id)
    {
        $m = self::MODEL;
        $model = $m::find($id);
        if(is_null($model)){
            return $this->responseError('not_found',$id, $m);
        }else{
            $model->is_active = "0";
            $model->save();
            return $this->responseSuccess('removed',$model, $m);
        }
    }

    public function massiveDeleteById(Request $request)
    {
        $m = self::MODEL;
        $m::whereIn('id',json_decode($request->getContent()))->update([
            'is_active' => '0',
        ]);
        return $this->responseSuccess('removed',json_decode($request->getContent()),$m);
    }

    protected function responseSuccess($status, $data = [], $model, $messages = null, $error = false)
    {
        $responseData = [
            'data' => [
                'attr' => ['entity' => $model, 'record' => $data],
                'code' => $this->statusCodes[$status],
                'message' => is_null($messages) ? $status: $messages],

        ];
        return response()->json($responseData);
    }

    protected function responseError($status, $data = [], $model, $messages = null, $error = false)
    {
        $responseData = [
            'data' => [
                'is_error' => [],
                'has_error' => [
                    'code' => $this->statusCodes[$status],
                    'message' => is_null($messages) ? $status: $messages,
                    'detail' => is_null($messages) ? $status: $messages
                ]
            ]
        ];
        return response()->json($responseData);
    }

}
