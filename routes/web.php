<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api'], function () use ($router) {

    $router->delete('users/massiveDeleteById', ['uses' => 'UsersController@massiveDeleteById']);

    $router->get('users', ['uses' => 'UsersController@all']);

    $router->get('users/{id}', ['uses' => 'UsersController@read']);

    $router->post('users', ['uses' => 'UsersController@create']);

    $router->put('users/{id}', ['uses' => 'UsersController@update']);

    $router->delete('users/{id}', ['uses' => 'UsersController@delete']);


});
